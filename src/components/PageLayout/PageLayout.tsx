import React from "react";

import Header from "@/components/Header/Header";
import DynamicBlock from "@/components/DynamicBlock/DynamicBlock";

import { projectStructure } from "@/utils/api";

import styles from './PageLayout.module.scss';

const PageLayout = () => {
    return (
        <main>
            <Header/>
            <div className={styles.body}>
                {projectStructure.map((block, index) =>
                    <DynamicBlock
                        key={index}
                        {...block}
                    />
                )}
            </div>
        </main>
    )
}

export default PageLayout;