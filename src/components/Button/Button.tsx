import React from 'react'
import styles from './Button.module.scss';

interface IProps {
  text: string;
}

const Button: React.FC<IProps> = ({ text }) => {
  return (
    <div className={styles.button}>
        <div className={styles.button_text}>{text}</div>
    </div>
  )
}

export default Button;