import React, { useMemo, useState } from 'react';

import { projectStructure } from '../../utils/api.js';
import Image from '@/components/Image';

import styles from './Header.module.scss';
import classNames from 'classnames';

export default function Header() {
  const [opened, setOpened] = useState(false);

  const showedInNavbar = useMemo(() => {
    return projectStructure.filter((el) => !!el.navName)
  }, []);

  return (
    <div className={styles.header}>
        <img className={styles.header__logo} src="/assets/eye.gif" alt=""/>

        <button className={styles.header__burger} onClick={() => setOpened(!opened)}>
          {
            opened ? (
              <Image src='/assets/cross.svg' alt=""/>
            ) : (
              <Image src='/assets/menu.svg' alt=""/>
            )
          }
        </button>

        <div
          className={classNames(
            styles.header__links,
            {
              [styles.header__links_opened]: opened
            }
          )}
        >
          {showedInNavbar.map((block, index) => (
            <a 
              key={index} 
              className={classNames(styles.header__link, {
                [styles.header__link_primary]: block.navPrimary
              })} 
              href={`#${block.id}`}
              onClick={()=>setOpened(false)}
            >
              {block.navName}
            </a>
          ))}
        </div>
    </div>
  )
}
