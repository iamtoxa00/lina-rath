import React from 'react'

import Button from '@/components/Button';
import Image from '@/components/Image';

import styles from './FigmaCard.module.scss'

interface IProps {
  name: string;
  descr: string;
  img: string;
  btnText: string;
  link: string;
}

const FigmaCard: React.FC<IProps> = ({
  name,
  descr,
  img,
  btnText,
  link
}) => {
  return (
    <div className={styles.figmaCard}>
      <Image className={styles.figmaCard__image} src={img} alt=""/>
      <div className={styles.figmaCard__infoWrapper}>
        <div className={styles.figmaCard__name}>
          {name}
        </div>
        <div className={styles.figmaCard__descr}>
          {descr}
        </div>
        <a href={link}>
          <Button text={btnText}/>
        </a>
      </div>
    </div>
  )
}

export default FigmaCard;
