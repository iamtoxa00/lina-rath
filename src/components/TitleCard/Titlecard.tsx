import React, {useState, useCallback, useEffect} from 'react';

import Button from '../Button/Button';

import styles from './Titlecard.module.scss';

interface IProps {
    content: {
        options: string[];
        title: string;
        link: string;
    }
}

const TitleCard: React.FC<IProps> = ({
    content: { options, title, link }
}) => {
    const [currentTitle, setCurrentTitle] = useState(options[0]);
    const [titleCounter, setTitleCounter] = useState(0)

    const shuffle = useCallback(() => {
        setTitleCounter(titleCounter +1)
        setCurrentTitle(options[titleCounter]);
    }, [options, titleCounter]);

    useEffect(() => {
        const titleInterval = setInterval(shuffle, 3000);

        if(titleCounter === 4) {
            setTitleCounter(1)
        }

        return () => {
            clearInterval(titleInterval)
        };
    }, [shuffle, titleCounter])
    
  return (
    <div className={styles.titlecard}>
        <div className={styles.titlecard__info}>
            <div className={styles.titlecard__textbox}>
                <span className={styles.titlecard__header}>
                    {title}
                </span>
                <span className={styles.titlecard__titles}>
                    {currentTitle}
                </span>
            </div>
            <a href={link}>
                <Button text="VIEW CV"/>
            </a>
        </div>
    </div>
  )
}

export default TitleCard;