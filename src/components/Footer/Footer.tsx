import Image from '@/components/Image';
import styles from './Footer.module.scss';

interface IProps {
    content: {
        title: string;
        email: string;
        socials: {
            dribble: string;
            linkedin: string;
            instagram: string;
        };
        image: string;
    };
    id?: string;
}

const Footer: React.FC<IProps> = ({
    content: {
        title,
        email,
        image,
        socials: {
            dribble,
            linkedin,
            instagram
        }
    },
    id
}) => {
    return (
        <div className={styles.footer}>
            <a id={id}/>
            <Image src={image} alt="" className={styles.footer__image} />
            <div className={styles.footer__container}>
                <span className={styles.footer__title}>{title}</span>
                <div className={styles.footer__contacts}>
                    <a href={`mailto:${email}`}>
                        <span className={styles.footer__email}>{email}</span>
                    </a>
                    <div className={styles.footer__socials}>
                        <a href={dribble} className={styles.footer__social}>
                            <Image src='/assets/dribble.svg' alt='dribble'/>
                        </a>
                        <a href={linkedin} className={styles.footer__social}>
                            <Image src='/assets/linkedin.svg' alt='linkedin'/>
                        </a>
                        <a href={instagram} className={styles.footer__social}>
                            <Image src='/assets/instagram.svg' alt='instagram'/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;