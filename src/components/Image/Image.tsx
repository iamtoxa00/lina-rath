/* eslint-disable @next/next/no-img-element */

interface IProps {
    className?: string;
    src: string;
    alt?: string;
}

const Image: React.FC<IProps> = ({
    alt = '',
    src,
    className
}) => {
    return (
        <img
            alt={alt}
            src={src}
            className={className}
        />
    )
}

export default Image;