import CardsContainer from "@/components/CardsContainer";
import CarouselSection from "@/components/CarouselSection";
import Footer from "@/components/Footer";
import TitleCard from "@/components/TitleCard";

const DynamicBlock: React.FC<any> = ({ type, ...data }) => {
    if(type === 'cards') {
        return (
            <CardsContainer {...data} />
        )
    }

    if(type === 'carousel') {
        return (
            <CarouselSection {...data} />
        )
    }

    if(type === 'title') {
        return (
            <TitleCard {...data} />
        )
    }

    if(type === 'footer') {
        return (
            <Footer {...data} />
        )
    }

    return null;
}

export default DynamicBlock;