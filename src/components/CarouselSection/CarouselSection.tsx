import classNames from 'classnames';
import Image from '@/components/Image';
import { useEffect, useRef, useState } from 'react';
import styles from './CarouselSection.module.scss'
import { isElementInViewport } from '@/utils/itemInViewport';

interface IGroup {
    name: string;
    text: string;
    items: string[][];
}

interface IProps {
    name?: string;
    content: IGroup[];
    id?: string;
    wideItems?: boolean;
    animation: 'fade' | 'slide';
}

const Carousel: React.FC<IProps> = ({
    name,
    content,
    id,
    wideItems,
    animation = 'slide'
}) => {
    const carouselRef = useRef<HTMLDivElement[]>([])
    const [position, setPosition] = useState(content.map(() => 0));
    const [screenSize, setScreenSize] = useState(768);
    
    useEffect(() => {
        if(typeof window != 'undefined') {
            setScreenSize(window.innerWidth)
        }
    }, [])

    const wideItemsWithScreen = wideItems || screenSize <= 768;

    const itemsPerSlide = wideItemsWithScreen ? 1 : 2;
    const slidesCount = content.map((el) => Math.ceil(el.items.length / itemsPerSlide));

    useEffect(() => {
        if (carouselRef.current) {
            carouselRef.current.forEach((_, groupIndex) => {
                const element = carouselRef.current[groupIndex];
                const slide = position[groupIndex];
                const newPos = slide * element.getBoundingClientRect().width + slide * 16;

                element.scrollTo({
                    behavior: 'smooth',
                    left: newPos
                })
            })
        }
    }, [position])

    const handleNext = (groupIndex: number) => () => {
        const newState = [...position];
        newState[groupIndex] = Math.min(slidesCount[groupIndex] - 1, Math.max(0, newState[groupIndex] + 1));
        setPosition(newState);
    }

    const handlePrev = (groupIndex: number) => () => {
        const newState = [...position];
        newState[groupIndex] = Math.min(slidesCount[groupIndex] - 1, Math.max(0, newState[groupIndex] - 1));
        setPosition(newState);
    }

    return (
        <div className={styles.carouselSection} id={id}>
            <span className={styles.carouselSection__heading}>
                {name}
            </span>
            <div className={styles.carouselSection__content}>
                {content.map((el, groupIndex) => (
                    <div key={groupIndex} className={styles.carouselSection__group}>
                        <span className={styles.carouselSection__groupTitle}>{el.name}</span>
                        <span className={styles.carouselSection__groupText}>{el.text}</span>

                        <div className={styles.carouselSection__carouselWrapper}>
                            <div className={styles.carouselSection__overlay}>
                                {position[groupIndex] !== 0 && (
                                    <button
                                        onClick={handlePrev(groupIndex)}
                                        className={classNames(
                                            styles.carouselSection__carouselArrow,
                                            styles.carouselSection__carouselArrow_left
                                        )}
                                    >
                                        <Image src={"/assets/button-chevron.svg"} alt="" />
                                    </button>
                                )}
                                {position[groupIndex] !== slidesCount[groupIndex] - 1 && (
                                    <button
                                        onClick={handleNext(groupIndex)}
                                        className={classNames(
                                            styles.carouselSection__carouselArrow,
                                            styles.carouselSection__carouselArrow_right
                                        )}
                                    >
                                        <Image src={"/assets/button-chevron.svg"} alt="" />
                                    </button>
                                )}
                            </div>
                            <div className={styles.carouselSection__carousel} ref={(el) => {
                                if (el) carouselRef.current[groupIndex] = el
                            }}>
                                {el.items.map((imageGroup, ItemIndex) => (
                                    <div
                                        key={ItemIndex}
                                        className={classNames(styles.carouselSection__carouselItem, {
                                            [styles.carouselSection__carouselItem_wide]: wideItemsWithScreen,
                                            [styles.carouselSection__carouselItem_fadeAnim]: ItemIndex > 0 && animation == 'fade',
                                            [styles.carouselSection__carouselItem_active]: ItemIndex == position[groupIndex]
                                        })}
                                    >
                                        {imageGroup.map((imageSrc, imageIndex) => {
                                            const isVideo = imageSrc.endsWith(".mp4");
                                            if(isVideo) {
                                                return (
                                                    <video
                                                        key={imageIndex}
                                                        src={imageSrc}
                                                        className={styles.carouselSection__carouselImage}
                                                        autoPlay
                                                        muted
                                                        controls
                                                    />
                                                )
                                            } else {
                                                return (
                                                    <Image
                                                        key={imageIndex}
                                                        src={imageSrc}
                                                        alt=""
                                                        className={styles.carouselSection__carouselImage}
                                                    />
                                                )
                                            }
                                        })}
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Carousel;