import FigmaCard from "@/components/FigmaCard";

import styles from './CardsContainer.module.scss';

interface IProps {
    name?: string;
    content: any[];
    id?: string;
}

const CardsContainer: React.FC<IProps> = ({
    name,
    content,
    id,
}) => {
    return (
        <div className={styles.cardsContainer} id={id}>
            <span className={styles.cardsContainer__heading}>
                {name}
            </span>
            <div className={styles.cardsContainer__content}>
                {content.map((el, index) => (
                    <FigmaCard
                        key={index}
                        {...el}
                    />
                ))}
            </div>
        </div>
    )
}

export default CardsContainer;