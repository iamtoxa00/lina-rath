import PageLayout from '@/components/PageLayout'
import Head from 'next/head'

export default function Home() {
  return (
    <>
      <Head>
        <title>Lina Rath Design</title>
        <meta name="description" content="Personal portfolio website" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <PageLayout />
      </main>
    </>
  )
}
