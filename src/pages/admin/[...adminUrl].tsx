import { AdminRouter } from "@/mdpanel";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useMemo } from "react";

const AdminPanel: NextPage = (props) => {
  const router = useRouter()
  const { adminUrl } = router.query

  const url = useMemo(() => {
    if(typeof adminUrl == 'string') {
      return '/'+adminUrl;
    }
    if(typeof adminUrl == 'object') {
      return '/'+adminUrl.join('/')
    }
    return '/'
  }, [adminUrl])

  return (
    <AdminRouter url={url} />
  )
}

export default AdminPanel;