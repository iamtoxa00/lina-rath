export const projectStructure = [
    {
        type: 'title',
        content: {
            title: "Hi there! I'm Lina,",
            options: [
                'a UI designer',
                'a concept artist',
                'a movie geek',
                'a UI designer'
            ],
            link: '/assets/files/CV.pdf',
        }
    },
    {
        name: 'User Interface',
        navName: 'UI Design',
        type: 'cards',
        id: 'ui',
        content: [
            {
                name: 'Litrar Audiobooks (Mobile App)',
                descr: 'A mobile app design of an audiobook app',
                link: 'https://www.figma.com/file/4tfauM9BVUh7M52qT0xaJn/Litrar-Audiobooks?node-id=0%3A1&t=deBOtHNzzAt6Fv8l-1',
                img: '/assets/works/ui-design/0.png',
                btnText: 'FIGMA FILE'
            },
            {
                name: 'Suni Sneaker Shop (Mobile App)',
                descr: 'A mobile app design of a sneaker shop',
                link: 'https://www.figma.com/file/yiacBgMGYm8PffmaNubDZk/SUNI-(SneakerShop)?node-id=0%3A1&t=dKVWTFeAM8rdykUg-1',
                img: '/assets/works/ui-design/1.png',
                btnText: 'FIGMA FILE'
            },
            {
                name: 'Web Landing Design Examples',
                descr: 'A few examples of web landing designs',
                link: 'https://www.figma.com/file/6ccQz4qp42PWGm8cYTqUjQ/Web-Examples?node-id=0%3A1&t=6SBxmKxvwSHs9aRq-1',
                img: '/assets/works/ui-design/2.png',
                btnText: 'FIGMA FILE'
            },
            {
                name: 'Mobile Apps Design Examples',
                descr: 'A few examples of mobile app designs',
                link: 'https://www.figma.com/file/PSoBnwoWZ40XxBKX6uz8vJ/App-Examples?node-id=0%3A1&t=nuOB8ocZNfZ9HFKO-1',
                img: '/assets/works/ui-design/3.png',
                btnText: 'FIGMA FILE'
            },
        ]
    },
    {
        name: 'Theater Concepts',
        navName: 'Theater Concepts',
        id: 'theater',
        type: 'carousel',
        content: [
            {
                name: "Blacklight Retrofuture",
                text: "Classical sci-fi retrofuturistic movies remastered in blacklight theatrical scenes",
                items: [
                    ['/assets/carousel/retrolfuture/1_1.png', '/assets/carousel/retrolfuture/1_2.png'],
                    ['/assets/carousel/retrolfuture/2_1.png', '/assets/carousel/retrolfuture/2_2.png'],
                    ['/assets/carousel/retrolfuture/3_1.png', '/assets/carousel/retrolfuture/3_2.png'],
                    ['/assets/carousel/retrolfuture/4_1.png', '/assets/carousel/retrolfuture/4_2.png']
                ],
            },

            {
                name: "Tribal Phantasmagoria",
                text: "Blacklight theatrical scenes showing ritual costumes and dances from all over the world",
                items: [
                    ['/assets/carousel/tribal-phantasmagoria/1.png'],
                    ['/assets/carousel/tribal-phantasmagoria/2.png'],
                    ['/assets/carousel/tribal-phantasmagoria/3.png'],
                    ['/assets/carousel/tribal-phantasmagoria/4.png']
                ],
            },

            {
                name: "Colorful Fauna",
                text: "Colorful psychedelic interpretations of the animal kingdom",
                items: [
                    ['/assets/carousel/colorful-fauna/1.png'],
                    ['/assets/carousel/colorful-fauna/2.png'],
                    ['/assets/carousel/colorful-fauna/3.png'],
                    ['/assets/carousel/colorful-fauna/4.png']
                ],
            },

            {
                name: "Faces",
                text: "Blacklight theatrical scenes accentuating faces (or parts of the human features)",
                items: [
                    ['/assets/carousel/faces/1.png'],
                    ['/assets/carousel/faces/2.png'],
                    ['/assets/carousel/faces/3.png'],
                    ['/assets/carousel/faces/4.png']
                ],
            }
        ]
    },
    
    {
        name: 'Exhibitions',
        navName: 'Exhibitions',
        id: 'exhibitions',
        type: 'carousel',
        content: [
            {
                name: "Fractals in Nature",
                text: "A museum exhibition concept inspired by the book of Benoît Mandelbrot “The Fractal Geometry of Nature“",
                items: [
                    ['/assets/carousel/fractals/1.gif'],
                    ['/assets/carousel/fractals/2_1.png', '/assets/carousel/fractals/2_2.png'],
                    ['/assets/carousel/fractals/3_1.png', '/assets/carousel/fractals/3_2.png'],
                    ['/assets/carousel/fractals/4_1.png', '/assets/carousel/fractals/4_2.png'],
                    ['/assets/carousel/fractals/5_1.png', '/assets/carousel/fractals/5_2.png'],
                    ['/assets/carousel/fractals/6_1.gif', '/assets/carousel/fractals/6_2.png'],
                    ['/assets/carousel/fractals/7_1.png', '/assets/carousel/fractals/7_2.png'],
                    ['/assets/carousel/fractals/8_1.png', '/assets/carousel/fractals/8_2.png']
                ],
            },

            {
                name: "BYOB Eternal Trip",
                text: "A generative art installation created using Touchdesigner",
                items: [
                    ['/assets/carousel/byob-slider/1.png'],
                    ['/assets/carousel/byob-slider/byob.mp4'],
                    ['/assets/carousel/byob-slider/3.png'],
                    ['/assets/carousel/byob-slider/4.png']
                ],
            },
        ]
    },

    {
        name: 'Event Concepts',
        navName: 'Event Concepts',
        type: 'cards',
        id: 'events',
        content: [
            {
                name: "'80s Week",
                descr: 'An event concept of a thematic festival',
                link: '/assets/files/80s-week.pdf',
                img: '/assets/works/eventconcepts/0.png',
                btnText: 'VIEW PDF'
            },
            {
                name: 'VR Fest',
                descr: 'A concept of a digital expo',
                link: '/assets/files/VR FEST.pdf',
                img: '/assets/works/eventconcepts/1.png',
                btnText: 'VIEW PDF'
            },
        ]
    },

    {
        name: 'Lighting Design',
        navName: 'Lighting Design',
        id: 'lightning',
        type: 'carousel',
        animation: 'fade',
        wideItems: true,
        content: [
            {
                name: "Pokémon Internet Cafe",
                text: "Adaptive lighting scenarios of a Japanese cartoon stylized Internet cafe",
                items: [
                    ['/assets/carousel/pokemon-cafe/1.png'],
                    ['/assets/carousel/pokemon-cafe/2.png'],
                    ['/assets/carousel/pokemon-cafe/3.png']
                ],
            },
        ]
    },

    {
        type: 'footer',
        navName: 'Contact',
        id: "contact",
        navPrimary: true,
        content: {
            title: "Thanks for viewing my portfolio!",
            email: "lina.rath.design@gmail.com",
            socials: {
                dribble: 'https://dribbble.com/acid_perception',
                linkedin: 'https://www.linkedin.com/in/lina-rath-621709230/',
                instagram: 'https://www.instagram.com/acid_perception/'
            },
            image: '/assets/profile.gif'
        }
    },
]