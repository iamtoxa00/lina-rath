interface IProps {
    url: string;
}

const AdminRouter: React.FC<IProps> = ({
    url
}) => {
    return (
        <main>
            {url}
        </main>
    )
}

export default AdminRouter;